/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartmustache',
  version: '3.0.2',
  description: 'templates done right'
}
